# RateLimiter

This is a NodeJS rate limiter for Express.

# Installation
`npm install`

# Run tests
`npm test`

# Architecture
This rate limiter is designed to only allow 10 (default) connections to a resource by a specific IP address during a 1 minute (default) time window.

It keeps a queue of the last 10 requests to a resource from an IP address in memory. For each request, the list is checked, culled of old requests if necessary, and a new request added to the queue. When a resource has been accessed 10 times in 1 minute, a 401 Unauthorized HTTP response is returned. Once 1 minute has passed, the old requests will be dequeued from the queue and the resource will be served.

If a rate limited IP address requests a different resource, a new rate limit queue is established and the rate limit is invoked independently of the first request.

# Usage
Use in middleware as such:
    
    let limiter = require('./limiter')().middleware;
    app.use(limiter);