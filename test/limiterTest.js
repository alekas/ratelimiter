
process.env.NODE_ENV = 'test';

let limiter = require('../limiter')().checkLimit;

let chai = require('chai');
let chaiHttp = require('chai-http');
let www = require('../bin/www');
let should = chai.should();
let mocha = require('mocha');
let describe = mocha.describe;
let it = mocha.it;

chai.use(chaiHttp);

//The below tests are REAL HTTP tests
describe('Basic smoke test', () => {
    it('It should get a 200 Ok response', (done) => {
        //just making sure we get a 200 on a basic request
        chai.request(www)
            .get('/get/testData1')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            })
    })
});

describe('Hit rate limit', () => {
    it('It should get 10 200s and then a 401 response', async () => {
        //Should get 10 200s
        for(let i = 0; i < 10; i++) {
            let res = await chai.request(www)
                .get('/get/testData2');
            res.should.have.status(200);
        }

        //11th should be a 401
        let res = await chai.request(www)
            .get('/get/testData2');
        res.should.have.status(401);
    })
});

describe('Change url after limit', () => {
    it('It should get 10 200s, then 1 401, then, for a different parameter, get 10 200s and 1 401', async () => {
        //Hit rate limit on first url
        for(let i = 0; i < 10; i++) {
            let res = await chai.request(www)
                .get('/get/testData3');
                res.should.have.status(200);
        }

        //show 401 for 11th call on first url
        let res = await chai.request(www)
            .get('/get/testData3');
        res.should.have.status(401);

        //change to a second url to show our rate limiter is url specific, should get 10 calls here...
        for(let i = 0; i < 10; i++) {
            let res = await chai.request(www)
                .get('/get/testData3Different');
                res.should.have.status(200);
        }

        //...and then a 401 on call 11 to the second url
        res = await chai.request(www)
            .get('/get/testData3Different');
        res.should.have.status(401);
    })
});


describe('Hit rate limit, wait for expiration, try again [THIS IS A 60s TEST]', () => {
    it('It should get 10 200s and then a 401 response, then 59 seconds later get another 401, and then wait 1 more second and get a 200', async () => {
        //get 10 200 OK responses
        for(let i = 0; i < 10; i++) {
            let res = await chai.request(www)
                .get('/get/testData4');
            res.should.have.status(200);
        }

        //Rate limit reached, get 401
        let res = await chai.request(www)
            .get('/get/testData4');
        res.should.have.status(401);

        //wait 59 seconds just to prove we have a 60 second timout window
        await delay(59000);

        //should still be a 401
        res = await chai.request(www)
            .get('/get/testData4');
        res.should.have.status(401);

        //after 1 more second, our first 10 calls will expire, so now we should have 200 OK
        await delay(1000);

        //200 OK
        res = await chai.request(www)
            .get('/get/testData4');
        res.should.have.status(200);
    }).timeout(62000);
});

//These tests are not real HTTP tests but test the limiter functionality from different IP addresses
describe('Test case simulation from challenge email from Austin and Chris', ()=> {
    it('It should allow 10 calls from 1.1.1.1, deny the next one from 1.1.1.1, allow from 2.2.2.2, wait until second 60 and accept from 1.1.1.1', async () => {
        for(let i = 0; i < 10; i++) {
            let allow = limiter('1.1.1.1', '/get/fileA.jpg');
            allow.should.not.equal(false);
            await delay(1000);
        }

        let allow = limiter('1.1.1.1', '/get/fileA.jpg');
        allow.should.equal(false);
        await delay(1000);

        allow = limiter('1.1.1.1', '/get/fileB.jpg');
        allow.should.not.equal(false);
        await delay(1000);

        allow = limiter('2.2.2.2', '/get/fileA.jpg');
        allow.should.not.equal(false);
        await delay(49000);

        allow = limiter('1.1.1.1', '/get/fileA.jpg');
        allow.should.not.equal(false);
    }).timeout(62000);
});


//just a quick and dirty delay method.
async function delay(time) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve();
        }, time);
    });
};