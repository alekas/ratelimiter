
function Limiter(options) {
    options = Object.assign(
        {
            requestLimit: 10,
            windowTime: 60 * 1000
        },
        options
    );
    let requests = {};
    let middleware = function(req, res, next) {
        let ip = req.connection.remoteAddress;
        let url = req.url;
        let allow = checkLimit(ip, url);
        if(allow) {
            next();
        } else {
            res.sendStatus(401);
            return false;
        }
    };
    let checkLimit = function(ip, url) {

        //define a unique id for this particular resource, which is defined as the URL and remote IP
        //this is used for keeping track of what endpoints the IP is hitting
        let requestId = ip + ':' + url;
        if(!requests[requestId]) {
            requests[requestId] = [];
        }

        //Let's clean out the queue of request older than the time limit we want
        while(requests[requestId].length && (new Date) - requests[requestId][0] > options['windowTime']) {
            requests[requestId].shift();
        }

        requests[requestId].push(new Date());

        if(requests[requestId].length > options['requestLimit']) {
            return false;
        }
        return(requests[requestId]);

    };

    return {
        checkLimit: checkLimit,
        middleware: middleware
    };
}

module.exports = Limiter;