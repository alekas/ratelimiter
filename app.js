var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var getRouter = require('./routes/get');
let limiter = require('./limiter')().middleware;

var app = express();
app.disable('etag');
app.use(logger('dev'));
app.use(limiter);


app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/get/', getRouter);



module.exports = app;
